## REACT SEARCH ENGINE

### Dev
`yarn start`
Go to `localhost:8080`.

### Build
`yarn build`
Check './dist'.

### Styleguide
`yarn styleguide`
Go to `localhost:6060`.

### Test
`yarn test`
Enjoy!