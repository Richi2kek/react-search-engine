module.exports = {
    sections: [
        {
            name: 'REACT SEARCH ENGINE',
            description: 'Made with ❤️ by <a href="https://gitlab.com/Richi2kek">Richi2kek</a>'
        },
        {
            name: 'Server',
            description: 'Documentation of the server part of the app',
            sections: [
                {
                    name: 'Introduction',
                    content: '../src/server/docs/intro.md'
                },
                {
                    name: 'Technologies',
                    content: '../src/server/docs/technos.md'
                },
                {
                    name: 'Features',
                    content: '../src/server/docs/features.md'
                },
                {
                    name: 'Bots',
                    sections: [
                        {
                            name: 'ChromeBot',
                            content: '../src/server/bots/docs/chrome-bot.md'
                        },
                        {
                            name: 'ChromeSpider',
                            content: '../src/server/bots/docs/chrome-spider.md'                            
                        }
                    ]
                }
            ]
        },
        {
            name: 'Client',
            descriptions: 'Documentation of the client part of the app',
            sections: [
                {
                    name: 'Introduction',
                    content: '../src/client/docs/intro.md'
                },
                {
                    name: 'Technologies',
                    content: '../src/client/docs/technos.md'
                },
                {
                    name: 'Features',
                    content: '../src/client/docs/features.md'
                },
                {
                    name: 'Components',
                    sections: [
                        {
                            name: 'Atoms',
                            components: '../src/client/components/atoms/**/*.jsx'
                        },
                        {
                            name: 'Molecules',
                            components: '../src/client/components/molecules/**/*.jsx'
                        },
                        {
                            name: 'Organisms',
                            components: '../src/client/components/organisms/**/*.jsx'
                        },
                        {
                            name: 'Templates',
                            components: '../src/client/components/templates/**/*.jsx'
                        },
                        {
                            name: 'Pages',
                            components: '../src/client/components/pages/**/*.jsx'
                        }
                    ]
                },
            ]
        }
    ],
    webpackConfig: require('./webpack.config.dev'),
    ignore: [
        '**/__tests__/**',
        '**/*.test.{js,jsx,ts,tsx}',
        '**/*.spec.{js,jsx,ts,tsx}',
        '**/*.d.ts',
        '**/components/index.{js,jsx,ts,tsx}'
    ],
};
