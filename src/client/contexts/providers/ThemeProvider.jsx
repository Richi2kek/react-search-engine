import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';

const ThemeContext = createContext('light');

export class ThemeProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: 'light'
        };
    }

    render() {
        return(
            <ThemeContext.Provider value={this.state.theme}>
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}

ThemeProvider.propTypes = {
    children: PropTypes.children
};