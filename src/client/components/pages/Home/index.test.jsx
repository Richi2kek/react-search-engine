import React from 'react';
import { shallow } from 'enzyme';

import { Home } from '../../';

describe('Home', () => {
    it('Should render the default view', () => {
        const wrapper = shallow(<Home/>);
        expect(wrapper).toMatchSnapshot();
    });
});