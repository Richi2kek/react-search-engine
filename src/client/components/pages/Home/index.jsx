import React, { Component } from 'react';

import spiderSocket from '../../../api/spiderSocket';

import {
    DefaultTemplate,
    D3ForceLayout,
    Input,
    Button
} from '../../index';

import './styles.scss';

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            data: {
                nodes: [],
                links: []
            },
            submitQuery: 'http://ynov.com',
            maxNbPages: 5
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmitQueryChange = this.handleSubmitQueryChange.bind(this);
        this.handleMaxNumberPagesChange = this.handleMaxNumberPagesChange.bind(this);

    }

    handleSubmit(e) {
        e.preventDefault();
        this.sendQuery(this.state.submitQuery, this.state.maxNbPages);
    }

    handleSubmitQueryChange(e) {
        this.setState({
            submitQuery: e.target.value
        });
    }

    handleMaxNumberPagesChange(e) {
        this.setState({
            maxNbPages: e.target.value
        });
    }

    sendQuery(url, nbPages) {
        spiderSocket.emit('submitQuery', {
            url,
            nbPages
        });
        spiderSocket.on('newPage', page => {
            this.createNodes(page);
        });
    }

    createNodes(page) {
        const nodes = [...this.state.data.nodes];
        const links = [...this.state.data.links];
        const newNode = {
            label: page.url,
            r: 50,
            id: nodes.length,
            links: page.links,
            // img: page.screenshotPath
        };
        nodes.push(newNode);
        nodes.forEach(node => {
            node.links.forEach(link => {
                const exist = nodes.filter(n => n.label === link);
                if (exist.length) {
                    const newLink = {
                        source: node.id,
                        target: exist.pop().id
                    };
                    links.push(newLink);
                }
            });
        });

        const data = { nodes, links };
        this.setState({ data });
    }

    render() {
        return (
            <DefaultTemplate
            >
                <D3ForceLayout
                    data={this.state.data}
                    height={750}
                    linksDistance={750}
                    linksForce={.1}
                />
                <form onSubmit={this.handleSubmit} className='search-form'>
                    <Input
                        className='search-input'
                        height='50px'
                        color='blueviolet'
                        placeholder='Search'
                        value={this.state.submitQuery}
                        onChange={this.handleSubmitQueryChange}
                    />
                    <Input
                        className='max-pages-input'                        
                        type='number'
                        placeholder='Max number pages'
                        value={this.state.maxNbPages}
                        onChange={this.handleMaxNumberPagesChange}
                    />
                    <Button
                        className='search-button'                        
                        type='submit'
                        color='blueviolet'
                        height={30}
                    >
                        Send
                    </Button>
                </form>
            </DefaultTemplate>
        );
    }
}
