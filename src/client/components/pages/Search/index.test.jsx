import React from 'react';
import { shallow } from 'enzyme';

import { Search } from '../../';

describe('Search', () => {
    it('Should render the Search bar', () => {
        const wrapper = shallow(<Search/>);
        expect(wrapper).toMatchSnapshot();
    });
});