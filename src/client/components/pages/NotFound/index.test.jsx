import React from 'react';
import { shallow } from 'enzyme';

import { NotFound } from '../../';

describe('NotFound', () => {
    it('Should render the Not Found page', () => {
        const wrapper = shallow(<NotFound/>);
        expect(wrapper).toMatchSnapshot();
    });
});