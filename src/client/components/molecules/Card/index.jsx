import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export class Card extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const themedClass = `default-card ${this.props.dark ? 'dark' : null}`;
        return (
            <div className={themedClass}>
                <div className="default-card-header">
                    <h1>{this.props.title}</h1>
                    <p>{this.props.subTitle}</p>
                </div>
                <img src={this.props.image} />
                <div className="default-card-content">
                    {this.props.children}
                </div>
                <div className="default-card-footer">
                    {this.props.actions}
                </div>
            </div>
        );
    }
}

Card.propTypes = {
    children: PropTypes.node,
    dark: PropTypes.bool,
    title: PropTypes.string,
    subTitle: PropTypes.string,
    image: PropTypes.string,
    actions: PropTypes.node
};