Card Component:

- It should render an 'div' tag container styled like a card.
```js
    <Card
        title="Hey"
        subTitle="How are you?"
        // image="/assets/image.svg"
        actions={
            <Button>Ok</Button>
        }
    >
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum omnis, ipsum, sequi dolorum iusto voluptates possimus totam pariatur itaque nulla soluta at aperiam dicta earum modi error minima rerum distinctio?</p>
    </Card>
```
