import React from 'react';
import { shallow } from 'enzyme';


import { Card } from '../../';

describe('<Card/>', () => {
    it('Should render a <div> tag as container styled like a card', () => {
        const wrapper = shallow(
            <Card dark
                title="Hey"
                subTitle="How are you?"
                image="/assets/image.svg"
                actions={
                    <button>Ok</button>
                }
            >
                <p>Hello world!</p>
            </Card>
        );
        expect(wrapper).toMatchSnapshot();
    });
});