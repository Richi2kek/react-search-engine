import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const StyledUl = styled.ul`
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #ffffff;
`;

const StyledLi = styled.li`
    float: ${props => props.horizontal ? 'left' : 'none'};
    &:hover {
        background-color: #f1f1f1;
    }
`;

const StyledLink = styled.a`
    display: block;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    height: 100%;
`;

export class NavigationList extends Component {
    render() {
        const links = this.props.links.map(link => (
            <StyledLi key={`${link.title}-navigation-link`} horizontal={this.props.horizontal}>
                <StyledLink href={link.href}>
                    {link.title}
                </StyledLink>
            </StyledLi>
        ));
        return(
            <StyledUl>
                {links}
            </StyledUl>
        );
    }
}


NavigationList.propTypes = {
    links: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            title: PropTypes.string
        })
    ),
    horizontal: PropTypes.bool,
};


NavigationList.defaultProps = {
    links: [],
    horizontal: false,
};