NavigationList Component:

- It should render a list of links.
```js
    <NavigationList
        links={[
            {
                href: '#',
                title: 'Home'
            },
            {
                href: '#',
                title: 'Contact'
            },
            {
                href: '#',
                title: 'About'
            },  
        ]}
    />
```

- It can be horizontal.
```js
    <NavigationList
        horizontal
        links={[
            {
                href: '#',
                title: 'Home'
            },
            {
                href: '#',
                title: 'Contact'
            },
            {
                href: '#',
                title: 'About'
            },  
        ]}
    />
```
