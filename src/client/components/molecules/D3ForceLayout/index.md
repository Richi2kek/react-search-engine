D3ForceLayout Component: 

```js
const data = {
    nodes: [
        {
            label: 'Richard',
            
            r: 50
        },
        {
            label: 'Coco',
            r: 50
        },
        {
            label: 'Kevin',
            r: 50
        },
    ],
    links: [
        {
            source: 0,
            target: 1
        },
        {
            source: 0,
            target: 2
        },
        {
            source: 1,
            target: 2
        }
    ]
};
<D3ForceLayout 
    data={data}
    height={750}
    linksDistance={250}
    linksForce={.75}
/>
```