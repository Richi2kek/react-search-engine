import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import { zoom } from 'd3-zoom';

import './styles.scss';


export class D3ForceLayout extends Component {
    constructor(props) {
        super(props);
        this.svg;
        this.chartLayer;
        this.simulation = d3.forceSimulation();
        this.node;
        this.link;
        this.state = {
            width: 0,
        };

        this._setSize = this._setSize.bind(this);
        this._draw = this._draw.bind(this);
        this._onWindowResize = this._onWindowResize.bind(this);
    }
    componentDidMount() {
        this._init();
        window.addEventListener('resize', this._onWindowResize);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.data !== this.props.data) {
            this._draw();
        }
    }

    render() {
        return (
            <div className='D3ForceLayout-container' ref={ref => this.container = ref}>
            </div>
        );
    }

    _init() {
        this.svg = d3.select(this.container).append('svg');
        this.chartLayer = this.svg.append('g').classed('chartLayer', true);
        this.svg.call(
            zoom()
                .scaleExtent([1 / 2, 8])
                .on('zoom', () => {
                    this.chartLayer.attr('transform', d3.event.transform);
                })
        );

        const width = this.container.clientWidth;
        this.setState({ width: width });
        this._setSize(width);
        this._initSimulation();
        this._initDraw();
    }

    _setSize(width) {
        this.svg
            .attr('width', width)
            .attr('height', this.props.height);

        this.chartLayer
            .attr('width', width)
            .attr('height', this.props.height);
    }

    _initSimulation() {
        this.simulation
            .force('charge', d3.forceManyBody())
            .force('center', d3.forceCenter(this.container.clientWidth / 2, this.props.height / 2))
            .force('y', d3.forceY(0))
            .force('x', d3.forceX(0))
            .on('tick', this._ticked.bind(this));
    }

    _initDraw() {
        this.link = this.chartLayer.append('g')
            .attr('class', 'links')
            .selectAll('line');

        this.node = this.chartLayer.append('g')
            .attr('class', 'nodes')
            .selectAll('circle');
    }

    _draw() {
        this.node = this.node.data(this.props.data.nodes, d => d.label);
        
        this.node.exit().remove();

        this.node = this.node.enter().append('circle')
            .attr('r', d => d.r)
            .attr('fill', 'blueviolet')
            .attr('stroke', 'black')
            .attr('stroke-width', '2px')
            .call(
                d3.drag()
                    .on('start', this._dragStarted.bind(this))
                    .on('drag', this._dragged.bind(this))
                    .on('end', this._dragEnded.bind(this))
            )
            .merge(this.node);

        this.node.append('title')
            .text(d => d.label);


        this.link = this.link.data(this.props.data.links);
        
        this.link.exit().remove();

        this.link = this.link.enter().append('line')
            .attr('stroke', 'black')
            .merge(this.link);


        this.simulation
            .force('link',
                d3.forceLink()
                    .id(d => d.index)
                    .distance(this.props.linksDistance)
                    .strength(this.props.linksStrength)
            )
            .force('collide', d3.forceCollide(d => d.r + 8).iterations(this.props.data.nodes.length));
        this.simulation
            .nodes(this.props.data.nodes);

        this.simulation.force('link')
            .links(this.props.data.links);

        this.simulation.restart();
    }


    _dragStarted(d) {
        if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    _dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    _dragEnded(d) {
        if (!d3.event.active) this.simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    _ticked() {
        this.link
            .attr('x1', d => d.source.x)
            .attr('y1', d => d.source.y)
            .attr('x2', d => d.target.x)
            .attr('y2', d => d.target.y);

        this.node
            .attr('cx', d => d.x)
            .attr('cy', d => d.y);
    }

    _onWindowResize() {
        const width = this.container.clientWidth;
        this.setState({ width: width });
        this._setSize(width);
    }
}

D3ForceLayout.propTypes = {
    data: PropTypes.shape({
        nodes: PropTypes.arrayOf(PropTypes.shape({
            label: PropTypes.string,
            r: PropTypes.number,
            id: PropTypes.number,
            links: PropTypes.arrayOf(PropTypes.string)
        })),
        links: PropTypes.arrayOf(PropTypes.any)
    }),
    height: PropTypes.number,
    linksDistance: PropTypes.number,
    linksStrength: PropTypes.number,
};

D3ForceLayout.defaultProps = {
    data: {
        nodes: d3.range(0, 100).map(d => {
            return {
                label: `l${d}`,
                r: ~~d3.randomUniform(8, 28)(),
                id: d,
                links: [
                    '1',
                    '2'
                ]
            };
        }),
        links: d3.range(0, 100).map(() => {
            return {
                source: ~~d3.randomUniform(100)(),
                target: ~~d3.randomUniform(100)()
            };
        })
    },
    height: 500,
    linksDistance: 75,
    linksStrength: 0.5
};
