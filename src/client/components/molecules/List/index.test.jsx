import React from 'react';
import { shallow } from 'enzyme';

import { List } from '../../';

describe('<List/>', () => {
    it('Should render an <ul> tag with childrens inside <li> tags', () => {
        const wrapper = shallow(
            <List>
                <div>1</div>
                <div>2</div>
                <div>3</div>
            </List>
        );
        expect(wrapper).toMatchSnapshot();
    });
});