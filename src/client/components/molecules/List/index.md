List Component:

- It should render an 'ul' tag with childrens as 'li'.
```js
    <List>
        <Card>1</Card>
        <Card>2</Card>
        <Card>3</Card>
    </List>
```
