import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export const List = props => {
    const children = React.Children.map(props.children, (child, i) => {
        return(
            <li key={i}>{child}</li>
        );
    });
    return(
        <ul className="default-list" {...props}>
            {children}
        </ul>
    );
};

List.propTypes = {
    children: PropTypes.node
};
