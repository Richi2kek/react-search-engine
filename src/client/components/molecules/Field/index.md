Field Component:

- It should render a Label and an Input Component.
```js
    <Field
        label="Password"
        color="lightblue"
        type="password"
        placeholder="Type your password here"
    />
```
