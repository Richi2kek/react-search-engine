import React from 'react';
import PropTypes from 'prop-types';

import {
    Label,
    Input
} from '../../';

export const Field = ({label, ...inputProps}) => (
    <div className="default-field">
        <Label>
            {label}
        </Label>
        <Input {...inputProps} />
    </div>
);

Field.propTypes = {
    label: PropTypes.string,
    inputProps: PropTypes.any
};
