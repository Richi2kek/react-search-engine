import React from 'react';
import { shallow } from 'enzyme';

import { Field } from '../../';

describe('<Field/>', () => {
    it('Should render a <div> tag with a <label> and an <input> tags', () => {
        const wrapper = shallow(
            <Field
                label="Password: "
                type="password"
            />
        );
        expect(wrapper).toMatchSnapshot();
    });
});