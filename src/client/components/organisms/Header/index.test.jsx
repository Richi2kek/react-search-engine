import React from 'react';
import { shallow } from 'enzyme';

import { Header } from '../../';

describe('Header', () => {
    it('Should render a <header> tag with Template component', () => {
        const wrapper = shallow(<Header></Header>);
        expect(wrapper).toMatchSnapshot();
    });
});