Header Component:

- It should render the default header
```js
    <Header
        logo="LOGO"
        links={[
            {
                href: '#',
                title: 'Home'
            },
            {
                href: '#',
                title: 'Contact'
            },
            {
                href: '#',
                title: 'About'
            },
        ]}
    />
```
