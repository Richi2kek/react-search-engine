import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    Link,
    NavigationList
} from '../../';

import './style.scss';

export  class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="default-header">
                <div className="default-header-logo">
                    <Link>{this.props.logo}</Link>
                </div>
                <div className="default-header-right">
                    <NavigationList horizontal
                        links={this.props.links}
                    />
                </div>
            </div>
        );
    }
}

Header.propTypes = {
    logo: PropTypes.node,
    links: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            title: PropTypes.string
        })
    )
};

Header.defaultProps = {
    links: []
};