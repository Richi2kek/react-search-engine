import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import {
    NavigationList
} from '../../';


const Wrapper = styled.div`
    height: 100%;
    width: ${props => props.open ? '50%' : '0'}
    position: relative;
    z-index: 1;
    top: 0;
    left: 0;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    border: solid black 1px;
`;

export class Sidenav extends Component {
    render() {
        return(
            <Wrapper {...this.props}>
                <NavigationList
                    links={this.props.links}
                />
            </Wrapper>
        );
    }
}

Sidenav.propTypes = {
    mode: PropTypes.string,
    side: PropTypes.string,
    open: PropTypes.bool,
    links: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            title: PropTypes.string
        })
    )
};

Sidenav.defaultProps = {
    mode: 'push',
    side: 'left',
    open: false,
    links: []
};