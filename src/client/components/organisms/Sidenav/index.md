Sidenav Component:

- It should render the default sidenav
```js
    initialState = { isOpen: true };
    <div
        style={{
            display: 'flex',
            border: 'solid black 1px',
            width: '100%',
            height: '350px',
            overflow: 'hidden'
        }}
    >
        <Sidenav
            open={state.isOpen}
            links={[
            {
                href: '#',
                title: 'Home'
            },
            {
                href: '#',
                title: 'Contact'
            },
            {
                href: '#',
                title: 'About'
            },
        ]}
        />
        <Button
            onClick={
                () => {
                    setState({
                        isOpen: !state.isOpen
                    });
                }
            }
        >
            Toggle Sidenav
        </Button>
    </div>
```
