import React from 'react';
import { shallow } from 'enzyme';

import { Footer } from '../../';

describe('Footer', () => {
    it('Should render a <footer> tag with Template component', () => {
        const wrapper = shallow(<Footer></Footer>);
        expect(wrapper).toMatchSnapshot();
    });
});