import React from 'react';
import { shallow } from 'enzyme';

import { DefaultTemplate } from '../../';

describe('DefaultTemplate', () => {
    it('Should render a Template for pages', () => {
        const wrapper = shallow(
            <DefaultTemplate
                header={
                    <div className="default-header">
                        header
                    </div>
                }
                footer={
                    <div className="default-footer">
                        footer
                    </div>
                }
            >
                <div className="default-content">
                    content
                </div>
            </DefaultTemplate>
        );
        expect(wrapper).toMatchSnapshot();
    });
});