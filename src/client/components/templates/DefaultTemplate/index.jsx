import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';


export class DefaultTemplate extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="default-template-container">
                <header className="default-template-header">
                    {this.props.header}
                </header>
                <nav className="default-template-navigation">
                    {this.props.navigation}
                </nav>
                <main className="default-template-main">
                    {this.props.children}
                </main>
                <footer className="default-template-footer">
                    {this.props.footer}
                </footer>
            </div>
        );
    }
}

DefaultTemplate.propTypes = {
    children: PropTypes.node,
    header: PropTypes.node,
    navigation: PropTypes.node,
    footer: PropTypes.node
};
