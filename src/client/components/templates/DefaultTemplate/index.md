DefaultTemplate Component:

- It should render the default template.
```js
    <DefaultTemplate
        header={
            <div style={{
                width: '100%',
                height: '100%',
                border: 'solid blueviolet 1px',
                textAlign: 'center'    
            }}
            >
                header
            </div>
        }
        navigation={
            <div style={{
                width: '100%',
                height: '100%',
                border: 'solid blue 1px',
                textAlign: 'center'            
            }}
            >
                navigation
            </div>
        }
        footer={
            <div style={{
                width: '100%',
                height: '100%',
                border: 'solid violet 1px',
                textAlign: 'center'            
            }}
            >
                footer
            </div>
        }
    >
        <div style={{
            width: '100%',
            height: '100%',
            border: 'solid red 1px',
            textAlign: 'center'
        }}
        >
            content
        </div>
    </DefaultTemplate>
```
