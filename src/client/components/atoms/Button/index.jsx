import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

const fontSize = props => `${props.height / 40}rem`;
const cursor = props => props.disabled ? 'not-allowed' : 'pointer';
const color = props => props.disabled ? 'rgba(66, 66, 66, 0.5) ': props.color;

const styles = css`
    font-size: ${fontSize};
    cursor: ${cursor};
    color: ${color};
    border: 2px solid ${color};
    display: inline-flex;
    align-items: center;
    white-space: nowrap;
    text-decoration: none;
    justify-content: center;
    // margin: 1em;
    padding: 0 1em;
    height: 2.5em;
    padding: 0.25em 1em;
    border-radius: 3px;
    background: transparent;
    border-radius: 3px;
    transition: 250ms ease-out;
    &:hover, &:focus, &:active {
        background-color: ${color};
        color: white;
    }
    &:focus {
        outline: none
    }
`;

const StyledButton = styled.button`${styles}`;

export class Button extends Component {
    render() {
        return(
            <StyledButton {...this.props} />
        );
    }
}


Button.propTypes = {
    disabled: PropTypes.bool,
    height: PropTypes.number,
    color: PropTypes.string
};

Button.defaultProps = {
    height: 40,
    color: 'rgb(66,66,66)'
};