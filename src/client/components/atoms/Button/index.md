Button Component:

- It should render a 'button' tag.
```js
    <Button>button</Button> 
```
- It can be resizable.
```js
    <Button height={25}>small</Button>
    <Button>medium</Button>
    <Button height={75}>large</Button>
```

- It can be stained.
```js
    <Button color="rgb(255,0,0)">red</Button>
    <Button color="blueviolet">blueviolet</Button>
    <Button color="#0000ff">blue</Button>
```

- It can be disabled.
```js
    <Button disabled>disabled</Button>
```