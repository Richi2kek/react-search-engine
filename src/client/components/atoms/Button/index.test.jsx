import React from 'react';
import { shallow } from 'enzyme';


import { Button } from '../../';

describe('<Button/>', () => {
    it('Should render a <button> tag', () => {
        const wrapper = shallow(<Button>OK</Button>);
        expect(wrapper).toMatchSnapshot();
    });
});