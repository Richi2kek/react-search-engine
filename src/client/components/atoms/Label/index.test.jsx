import React from 'react';
import { shallow } from 'enzyme';


import { Label } from '../../';

describe('<Label/>', () => {
    it('Should render a <label> tag', () => {
        const wrapper = shallow(<Label>Password: </Label>);
        expect(wrapper).toMatchSnapshot();
    });
});