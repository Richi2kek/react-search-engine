import React from 'react';
import styled, { css } from 'styled-components';


const styles = css`
    color: rgba(66,66,66);
    font-size: 1rem;
    line-height: 2em;
    font-weight: bold;
    &:after {
        content ": "
    }
`;

const StyledLabel = styled.label`${styles}`;

export const Label = props => <StyledLabel {...props} />;
