import React, { Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledSvg = styled.svg`
    widht: ${props => props.size};
    height: ${props => props.size};
`;

export class Svg extends Component {
    render() {
        return(
            <StyledSvg {...this.props}      
                viewBox={`0 0 ${this.props.size} ${this.props.size}`}
                preserveAspectRatio='xMinYMin meet'
            />
        );
    }
}

Svg.propTypes = {
    size: PropTypes.number
};

Svg.defaultProps = {
    size: 500
};
