Svg Component: 

It should render an 'svg' tag: 

```js
<Svg  size={250} style={{border: 'solid black 1px'}} >
    <circle fill='red' stroke='black' cx='125' cy='125' r='50'/>
</Svg>
```