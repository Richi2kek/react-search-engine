import React from 'react';
import { shallow } from 'enzyme';


import { Link } from '../../';

describe('<Link/>', () => {
    it('Should render a <a> tag', () => {
        const wrapper = shallow(<Link href="#">Home</Link>);
        expect(wrapper).toMatchSnapshot();
    });
});