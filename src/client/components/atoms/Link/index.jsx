import React from 'react';
import styled, { css } from 'styled-components';


const styles  = css`
    text-decoration: none;
    font-weight: 500;
    color: rgb(66, 66, 66);
    cursor: pointer;
    &:hover {
        text-decoration: underline;
        color: blueviolet;
    }
`;

const StyledLink = styled.a`${styles}`;

export const Link = props => <StyledLink {...props}/>;

