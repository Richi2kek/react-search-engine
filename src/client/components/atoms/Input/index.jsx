import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const color = props => props.color;
const cursor = props => props.disabled ? 'not-allowed' : 'default';

const styles = css`
    display: block;
    width: 100%;
    margin: 0;
    padding: 0 0.45em;
    height: 2.22em;
    color: rgb(66, 66, 66);
    border: 1px solid ${color};
    border-radius: 2px;
    cursor: ${cursor}
    &:focus {
        outline-color: ${color}
    }
`;

const StyledInput = styled.input`${styles}`;

export const Input = props => <StyledInput {...props}/>;

Input.propTypes = {
    disabled: PropTypes.bool,    
    color: PropTypes.string
};

Input.defaultProps = {
    disabled: false,
    color: 'rgb(66, 66, 66)'
};