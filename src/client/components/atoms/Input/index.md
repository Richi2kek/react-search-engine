Input Component:

- It should render an 'input' tag.
```js
    <Input type="password" placeholder="Pasword..."/> 
```
- It can be stained.
```js
    <Input color="red" placeholder="red"/>
    <br/>
    <Input color="green" placeholder="green"/>
    <br/>    
    <Input color="rgba(255, 0, 255, 0.7)" placeholder="rgba(255, 0, 255, 0.7)"/>    
```
- It can be disabled.
```js
    <Input placeholder="disabled" disabled />   
```
