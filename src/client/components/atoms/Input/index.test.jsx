import React from 'react';
import { shallow } from 'enzyme';

import { Input } from '../../';

describe('<Input/>', () => {
    it('Should render an <input> tag', () => {
        const wrapper = shallow(<Input type="password" placeholder="Password"/>);
        expect(wrapper).toMatchSnapshot();
    });
});