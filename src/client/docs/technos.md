
- Equip with <a href="https://reactjs.org/">react</a> v16, <a href="https://github.com/ReactTraining/react-router/tree/master/packages/react-router-dom">react-router-dom</a> v4 ⚛️
- Equip wit <a href="https://github.com/styled-components/styled-components">react-styled-components</a> 💅
- Equip wit <a href="#">d3</a>v4 
- Equip with <a href="https://www.apollographql.com/docs/react/">apollo-react</a> 🚀
- Equip with <a href="https://js.tensorflow.org/">tensorflow.js</a> 🧠 
- Build with <a href="https://webpack.js.org/">webpack</a> v4 <a href="https://babeljs.io/">babel</a> v6 🎁
- Styleguide with <a href="https://github.com/styleguidist/react-styleguidist">styleguidist</a> 🐙
- Unit test with <a href="https://facebook.github.io/jest/">jest</a> and <a href="https://github.com/airbnb/enzyme">enzyme</a> 🃏