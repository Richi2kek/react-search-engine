import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { ApolloProvider } from 'react-apollo';

import client from './graphql/client';

import {
    Home,
    Search,
    NotFound
} from './components';

export default class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <ApolloProvider client={client}>
                <Switch>
                    <Route exact={true} path="/" component={Home} />
                    <Redirect from="/home" to="/" />
                    <Route path="/search" component={Search} />
                    <Route component={NotFound} />
                </Switch>
            </ApolloProvider>
        );
    }
}