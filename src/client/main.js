import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './App';

import './global.scss';

ReactDOM.render(
    <Router>
        <App />
    </Router>
    , document.querySelector('#root')
);