const { buildSchema } = require('graphql');

const defaultSchema = buildSchema(`
  type Query {
    hello: String
  }
`);

module.exports = {
    defaultSchema
};