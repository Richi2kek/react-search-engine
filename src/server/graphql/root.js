const root = {
    hello: () => {
        return 'Hello world!';
    },
};

module.exports = {
    root
};
