const fs = require('fs');
const path = require('path');

const Spider = require('./Spider');

(async () => {
    const nodes = [];
    const spider = new Spider();
    spider.events.on('newPage', page => {
        console.log('New Page crawled');
        console.log(page.url);
        nodes.push(page);
    });
    await spider.start('http://www.ynov.com');
    fs.writeFileSync(path.resolve(__dirname, 'data', 'results.json'),  JSON.stringify(nodes));
    console.log('done');
})();