ChromeSpider is a hight-level browser automation library.

The goal is to expose a few simple synchronous methods for crawling the web.

#### Examples
_____________


```js static
    const Spider = require('ChromeSpider');
    const fs = require('fs');
    const path = require('path');

    (async () => {
        try {
            const spider = new Spider();
            const results = await spider.start('http://my-wesite.com');
            fs.writeFileSync(path.resolve(__dirname, 'data', 'my-website-results.json'), JSON.stringify(results));
        } catch (err) {
            throw err;
        }
    })();
```