ChromeBot is a hight-level browser automation library.

The goal is to expose a few simple synchronous methods that mimic user actions with an API.

#### Examples
_____________


```js static
const ChromeBot = require('ChromeBot');
(async () => {
    try {
        const bot = new ChromeBot({show: false});
        await bot.newTab('http://google.com');
        await bot.type('.gsfi', '');
        await bot.click('input[name="btnK"]');
        await bot.waitForPage();
        await bot.snapshot();
        await bot.exit();
    } catch (err) {
        throw err;
    }
})();
```

#### API
_________
<strong>ChromeBot(options)</strong>
Constructor
```js static
const chromeBot = new ChromeBot({
    show: false,
    port: 9222
});
```
<strong>newTab(default: 'about:blank')</strong>
Open a new tab, return the id of the tab
```js static
const tab = chromeBot.newTab('http://google.com');
```
<strong>closeTab(optional: id)</strong>
Close the current tab or tab with given id
```js static
chromeBot.closeTab(myTab);
```
<strong>goTo(default: 'http://duckduckgo.com')</strong>
Navigate to the given url
```js static
chromeBot.goTo('http://google.com');
```
<strong>reload()</strong>
Reload the current page
```js static
chromeBot.reload();
```
<strong>sleep(default: 3000)</strong>
Waith for a given time in ms
```js static
chromeBot.sleep(5000);
```
<strong>waitForPage()</strong>
Wait for page load event fired
```js static
chromeBot.waitForPage();
```
<strong>waitForElement(selector)</strong>
Wait for a given element
```js static
chromeBot.waitForElement('.container');
```
<strong>screenshot(default: true)</strong>
take a screenshot. 2 mode:
- landscape default
- portrait
```js static
const screenshot = chromeBot.screenshot();
```
<strong>pdf(default: true)</strong>
take a pdf. 2 mode:
- landscape default
- portrait
```js static
const pdf = chromeBot.pdf();
```
<strong>type(selector, sentence)</strong>
Type in the given input
```js static
chromeBot.type('.search-input', 'my search input');
```
<strong>click(selector)</strong>
Click on the given element
```js static
chromeBot.click('.button');
```
<strong>getRequests()</strong>
Sniff all pages requests
```js static
const requests = chromeBot.getRequests();
```
<strong>evaluate(fn, ...args)</strong>
Allow us to pass function into the dom and return the results
```js static
const metas = chromeBot.evaluate(() => {
    return new Promise(resolve => {
        const metas = [...document.querySelectorAll('meta')]
            .map(el => el.innerHTML);
        resolve(metas);
    });
});
```
<strong>exit()</strong>
Kill the bot and the chrome process linked to him.
```js static
chromeBot.exit()
```