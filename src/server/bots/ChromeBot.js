const ChromeLauncher = require('chrome-launcher');
const ChromeRemoteInterface = require('chrome-remote-interface');

// TODO Change agent
class ChromeBot {
    constructor(
        {
            port = 9222,
            show = true
        }
    ) {
        this.chrome = {
            port: port,
            flags: show ? ['--disable-gpu'] : ['--headless', '--disable-gpu'],
            instance: null,
            isRunning: false
        };
        this.targets = [];
        this.currentTarget = null;
        this.client = null;
    }
    /**
     * PUBLIC
     */
    /**
     * @description Create a new tab
     * * 1) if chrome is not running init chrome, target, client and navigate to startingUrl
     * * 2) else create a new Tab and attach target to the client then navigate to startingUrl
     * @property {String} startingUrl
     * @default about:blank
     * @returns {String} Target.id
     */
    async newTab(startingUrl = 'about:blank') {
        try {
            if (!this.chrome.isRunning) {
                await this._init();
                await this.client.Page.navigate({ url: startingUrl });
            } else {
                await ChromeRemoteInterface.New();
                await this._initTarget();
                await this._initClient();
                await this.client.Page.navigate({ url: startingUrl });
            }
            return this.currentTarget.id;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Close the tab by id
     * * 1) Close the current tab
     * * 2) Remove the target from saved targets
     * @param {String} id
    */
    async closeTab(id = this.currentTarget.id) {
        try {
            await ChromeRemoteInterface.Close({ id });
            const index = this.targets.indexOf(target => target.id === id);
            this.targets.splice(index, 1);
            await this._initTarget();
            await this._initClient();
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Go to requested url
     * * 1) wait until page navigating to requested url
     * @param {String} startingUrl
     * @default http://duckduckgo.com
     */
    async goTo(startingUrl = 'http://duckduckgo.com') {
        try {
            await Promise.all([this.client.Network.enable(), this.client.Page.enable()]);

            await this.client.Page.navigate({ url: startingUrl });

        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Reload the current page
    */
    async reload() {
        try {
            await this.client.Page.reload();
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Wait in the page for the given time
     * @param {Number} ms
     */
    async sleep(ms = 3000) {
        try {
            await this.evaluate(ms => {
                return new Promise(resolve => {
                    setTimeout(() => {
                        resolve();
                    }, ms);
                });
            }, ms);
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Wait until page load event fired
     * * 1) enable page events
     * * 2) await until page loaded
     * * 3 disable page events
    */
    async waitForPage() {
        try {
            // await this.client.Page.enable();
            await this.client.Network.setCacheDisabled({cacheDisabled: true});
            await this.client.Page.loadEventFired();
            await Promise.all([this.client.Network.disable(), this.client.Page.disable()]);

            
            // await this.client.Page.disable();
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Wait for a specific element
     * * 1) enable page and network events
     * * 2) disable cache
     * * 3) evaluate the dom with mutation observer: disconnect when node with given selector find or when page load event fired
     * @param {String} selector
     */
    async waitForElement(selector) {
        try {
            await Promise.all([this.client.Network.enable(), this.client.Page.enable()]);
            await this.client.Network.setCacheDisabled({cacheDisabled: true});
            const result = await this.evaluate((selector) => {
                return new Promise((resolve, reject) => {
                    try {
                        const observer = new MutationObserver((mutations, observer) => {
                            const nodes = [];
                            mutations.forEach(mutation => {
                                nodes.push(...mutation.addedNodes);
                            });
                            nodes.find(node => {
                                if ([].indexOf.call([...document.querySelectorAll(selector)], node) !== -1) {
                                    observer.disconnect();
                                    resolve(true);
                                }
                            });
                            if (document.readyState === 'complete') {
                                const el = document.querySelector(selector);
                                if (el) {
                                    observer.disconnect();
                                    resolve(true);
                                } else {
                                    resolve(false);
                                }
                            }
                        });
                        observer.observe(document, {
                            childList: true,
                            subtree: true,
                            attributes: true,
                        });
                    } catch (err) {
                        reject(err);
                    }
                });
            }, selector);
            await Promise.all([this.client.Network.disable(), this.client.Page.disable()]);
            return result;
        } catch(err) {
            console.error(err);
        }
    }
    /**
     * @description Take a screenshot of the current page
     * @return {String} data {encoding: 'base64', ext: 'jpg', 'png', 'jpeg', 'svg'}
    */
    async screenshot() {
        try {
            const { data } = await this.page.captureScreenshot();
            return data;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description take a pdf of the current page
     * @param {boolean} landscape
     * @returns {String} data {encoding: 'bas64', ext: 'pdf'}
     */
    async pdf(landscape = true) {
        try {
            await this.page.enable();
            const { data } = await this.page.printToPDF({
                landscape,
                printBackground: true,
                marginTop: 0,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0
            });
            return data;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Type the given text in the selected input
     * @param {String} inputSelector
     * @param {String} text
     */
    async type(inputSelector, text) {
        try {
            await this.evaluate((selector, text) => {
                return new Promise((resolve, reject) => {
                    try {
                        const input = document.querySelector(selector);
                        input.value = text;
                        resolve();
                    } catch (err) {
                        reject(err);
                    }
                });
            }, inputSelector, text);
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Click on the selected element
     * @param {String} selector
     */
    async click(selector) {
        try {
            await this.evaluate((selector) => {
                return new Promise((resolve, reject) => {
                    try {
                        document.querySelector(selector).click();
                        resolve();
                    } catch (err) {
                        reject(err);
                    }
                });
            }, selector);
        } catch (err) {
            return err;
        }
    }
    /**
     * @description Sniff all request received by the current page
     * @returns {Array[Object]} requests
    */
    async getRequests() {
        try {
            await Promise.all([this.client.Network.enable(), this.client.Page.enable()]);
            const requests = await new Promise((resolve, reject) => {
                try {
                    const requests = [];
                    this.client.Network.requestWillBeSent((request) => {
                        requests.push(request);
                    });
                    this.client.Page.loadEventFired().then(() => {
                        resolve(requests);
                    });
                } catch (err) {
                    reject(err);
                }
            });
            await Promise.all([this.client.Network.disable(), this.client.Page.disable()]);
            return requests;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Evaluate the DOM
     * * 1) Stringify the function and the args if exists in an expression variable
     * * 2) evaluate the dom with the expression variable and wait for results
     * @param {function} fn
     * @param {any[]} args
     * @returns {any} value of evaluation in DOM
     */
    async evaluate(fn, ...args) {
        try {
            const expression = args && args.length > 0 ? `(${String(fn)}).apply(null, ${JSON.stringify(args)})` : `(${String(fn)}).apply(null)`;
            const { result: { value } } = await this.client.Runtime.evaluate({
                expression,
                returnByValue: true,
                awaitPromise: true
            });
            return value;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Terminate all
     * * 1) kill chrome process
    */
    async exit() {
        try {
            await this.chrome.instance.kill();
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * PRIVATES
     */
    /**
     * @description Init the bot
     * * 1) Launch chrome
     * * 2) get the context target
     * * 3) attach target to the client
     * @param {String} startingUrl
     * @default about:blank
     */
    async _init() {
        try {
            this.chrome.instance = await ChromeLauncher.launch({
                port: this.chrome.port,
                chromeFlags: this.chrome.flags,
            });
            await this._initTarget();
            await this._initClient();
            this.chrome.isRunning = true;
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * @description Init the target
     * * 1) List all available targets
     * * 2) get the current target
     * * 3) save it
    */
    async _initTarget() {
        try {
            const availableTarget = await ChromeRemoteInterface.List();
            this.currentTarget = availableTarget.filter(target => {
                if (target.type === 'page') {
                    if (!this.currentTarget) {
                        return target;
                    } else {
                        if (target.id !== this.currentTarget.id) {
                            return target;
                        }
                    }
                }
            }).pop();
            let exist = false;
            this.targets.forEach(target => {
                if (target.id === this.currentTarget.id) {
                    exist = true;
                }
            });
            if (!exist) {
                this.targets.push(this.currentTarget);
            }
        } catch (err) {//
            console.error(err);
        }
    }
    /**
     * @description Init the client
     * * 1) attach the target to the client
    */
    async _initClient() {
        try {
            this.client = await ChromeRemoteInterface({ target: this.currentTarget });
        } catch (err) {
            console.error(err);
        }
    }

}

module.exports = ChromeBot;