const EventEmitter = require('events').EventEmitter;
const URL = require('url').URL;

const ChromeBot = require('./ChromeBot');

class ChromeSpider extends ChromeBot {
    constructor() {
        super({
            show: false
        });
        this.baseUrl = '';
        this.pagesVisited = {};
        this.maxPagesToVisit = 0;
        this.nbPagesVisited = 0;
        this.pagesToVisit = [];
        this.results = [];
        this.events = new EventEmitter();
    }

    async start(url, maxPagesToVisit = 100) {
        url = new URL(url);
        const protocol = url.protocol;
        const hostname = url.hostname;
        this.maxPagesToVisit = maxPagesToVisit;
        await this._setBasetUrl(protocol, hostname);
        await this.newTab();
        await this.goTo(this.baseUrl);
        await this._crawl();
        await this.exit();
        return this.results;
    }

    async _setBasetUrl(protocol, hostname) {
        this.baseUrl = `${protocol}//${hostname}`;
        this.pagesToVisit.push(this.baseUrl);
    }

    async _crawl() {
        if (!this.pagesToVisit.length) {
            console.log('no more page to crawl');
            return;
        }
        if (this.nbPagesVisited >= this.maxPagesToVisit) {
            console.log('Max limit reached');            
            return;
        }
        const nextPage = this.pagesToVisit.pop();
        if (nextPage in this.pagesVisited) {
            await this._crawl();
        } else {
            const page = await this._scrapData(nextPage);
            await this.events.emit('newPage', page);
            this.results.push(page);
            await this._crawl();
        }
    }

    async _scrapData(nextPage) {
        this.pagesVisited[nextPage] = true;
        this.nbPagesVisited++;        
        await this.sleep(2000);
        const links = await this._getRelativeLinks();
        return {
            url: nextPage,
            links
        };
    }
    async _getRelativeLinks() {
        let relativeLinks = await this.evaluate(() => {
            return [...document.querySelectorAll('a[href^=\'/\'')]
                .map(link => link.getAttribute('href'));
        });
        // console.log(`Found ${relativeLinks.length} relatives links`);
        relativeLinks = relativeLinks.map(link => {
            const url = this.baseUrl + link;
            this.pagesToVisit.push(url);
            return url;
        });
        return relativeLinks;
    }
}

module.exports = ChromeSpider;