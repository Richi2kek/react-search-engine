const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const http = require('http');

const Spider = require('./bots/Spider');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.HTTP_PORT = process.env.HTTP_PORT || 3000;

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

app.set('env', process.env.NODE_ENV);
app.set('port', process.env.HTTP_PORT);

app.use(cors());
app.use(helmet());
app.use(bodyParser.json());



// Socket
io.on('connection', client => {
    console.log(`New client connected with id:${client.id}.`);

    client.on('submitQuery', async query => {
        console.log(`Client with id:${client.id} submit a query`);
        console.log(`Query: ${query}`);   
        const spider = new Spider();
        spider.events.on('newPage', page => {
            console.log('New Page crawled');
            console.log(page.url);
            client.emit('newPage', page);
        });
        await spider.start(query.url, query.nbPages);        
    });

    client.on('disconnect', () => {
        console.log(`Client with id:${client.id} disconnected.`);
    });
});


server.listen(process.env.HTTP_PORT, () => {
    console.log(`HTTP server is running on http://localhost:${process.env.HTTP_PORT}`);
});
